import XMonad
import XMonad.Layout.Grid
import XMonad.Layout.Spiral
import XMonad.Layout.Maximize
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.Spacing
import XMonad.Config.Xfce
import XMonad.Config.Desktop
import Data.List (intercalate)
import qualified Data.Map as M
import XMonad.Hooks.ManageDocks (avoidStruts)
import XMonad.Actions.FindEmptyWorkspace
import XMonad.Layout.SimpleFloat
import XMonad.Layout.PerWorkspace

main = xmonad myConfig

myConfig = xfceConfig { borderWidth = 2
               ,modMask = mod4Mask
               ,terminal = "urxvt"
               ,normalBorderColor = "#cccccc"
               ,focusedBorderColor = "#336699" -- blue
               ,layoutHook = avoidStruts myLayoutHook
               ,startupHook = startup <+> startupHook xfceConfig
               ,manageHook = myManageHook <+> manageHook xfceConfig
               ,keys = myKeys <+> keys xfceConfig
               }
myLayoutHook =
    -- spacing $
    onWorkspace "7" (simpleFloat ||| tiled) $
    maximize (tiled ||| Mirror tiled ||| Grid ||| spiral (6/7))
    ||| noBorders Full
    where tiled = Tall nmaster delta ratio
          nmaster = 1
          ratio = 1/2
          delta = 3/100
          spacing = spacingRaw True (Border 0 10 10 10)
                    True (Border 10 10 10 10) True

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList
            [ ((modm, xK_f),  withFocused (sendMessage . maximizeRestore))
              ,((modm, xK_r), spawn "/home/jordan/usr/bin/commit_to_rikai.sh")
              , ((mod4Mask,                xK_m    ), viewEmptyWorkspace)
              , ((mod4Mask .|. shiftMask,  xK_m    ), tagToEmptyWorkspace   )
            ]

startup :: X ()
startup = do spawn "xcompmgr"
             spawn "xmodmap ~/.xmodmap"

matchAny :: String -> Query Bool
matchAny x = foldr ((<||>) . (=? x)) (return False) [className
                                                    ,title
                                                    ,appName
                                                    ,resource]

myManageHook :: ManageHook
myManageHook = composeAll [ matchAny v --> a | (v,a) <- myActions]
    where myActions =
              [ ("Xfrun4"                           , doFloat)
                , ("Xfce4-appfinder"                , doFloat)
                , ("Application Finder"             , doFloat)
                , ("Xfce4-notifyd"                  , doIgnore)
                , ("MPlayer"                        , doFloat)
                , ("mpv"                            , doFloat)
                , ("Signal"                         , doFloat <+> doShift "2")
                , ("thunderbird"                    , doShift "8")
                , ("animation-SpriteTestWindow"     , doFloat)
                -- , ("gimp-image-window"              , (ask >>= doF . W.sink))
                -- , ("gimp-toolbox"                   , (ask >>= doF . W.sink))
                -- , ("gimp-dock"                      , (ask >>= doF . W.sink))
                , ("gimp-image-new"                 , doFloat)
                , ("gimp-toolbox-color-dialog"      , doFloat)
                , ("gimp-layer-new"                 , doFloat)
                , ("gimp-vectors-edit"              , doFloat)
                , ("gimp-levels-tool"               , doFloat)
                , ("preferences"                    , doFloat)
                , ("gimp-keyboard-shortcuts-dialog" , doFloat)
                , ("gimp-modules"                   , doFloat)
                , ("unit-editor"                    , doFloat)
                , ("screenshot"                     , doFloat)
                , ("gimp-message-dialog"            , doFloat)
                , ("gimp-tip-of-the-day"            , doFloat)
                , ("plugin-browser"                 , doFloat)
                , ("procedure-browser"              , doFloat)
                , ("gimp-display-filters"           , doFloat)
                , ("gimp-color-selector"            , doFloat)
                , ("gimp-file-open-location"        , doFloat)
                , ("gimp-color-balance-tool"        , doFloat)
                , ("gimp-hue-saturation-tool"       , doFloat)
                , ("gimp-colorize-tool"             , doFloat)
                , ("gimp-brightness-contrast-tool"  , doFloat)
                , ("gimp-threshold-tool"            , doFloat)
                , ("gimp-curves-tool"               , doFloat)
                , ("gimp-posterize-tool"            , doFloat)
                , ("gimp-desaturate-tool"           , doFloat)
                , ("gimp-scale-tool"                , doFloat)
                , ("gimp-shear-tool"                , doFloat)
                , ("gimp-perspective-tool"          , doFloat)
                , ("gimp-rotate-tool"               , doFloat)
                , ("gimp-open-location"             , doFloat)
                , ("gimp-file-open"                 , doFloat)
                , ("animation-playbac"              , doFloat)
                , ("gimp-file-save"                 , doFloat)
                , ("file-jpeg"                      , doFloat)
                , ("zenity"                         , doFloat)

            ]
