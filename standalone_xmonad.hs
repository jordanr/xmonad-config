import XMonad
import XMonad.ManageHook
import XMonad.Hooks.DynamicLog
-- import XMonad.Hooks.ManageDocks
-- import XMonad.Hooks.SetWMName
-- import XMonad.Util.Run (spawnPipe)
import XMonad.Util.EZConfig (additionalKeysP
                            ,additionalKeys
                            ,additionalMouseBindings
                            )
import XMonad.Layout.Grid
import XMonad.Layout.Spiral
import XMonad.Layout.Maximize
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.Spacing
-- import System.IO
-- import qualified Data.Map as M
-- -- import qualified System.IO.UTF8 (hPutStrLn)
import Data.List (intercalate)


main :: IO ()
main = xmonad =<< xmobar myConfig

-- myNormalConfig takes a Handle as an argument
myConfig = def { borderWidth = 2
               ,modMask = mod4Mask
               ,terminal = "urxvt"
               ,normalBorderColor = "#cccccc"
               ,focusedBorderColor = "#336699" -- blue
               --,startupHook = setWMName "LG3D"
               --, normalBorderColor = "#696969"
               --, focusedBorderColor = "#708090"
               --, focusedBorderColor = "#cd8b00" --orange
               , manageHook = myManageHook -- <+> manageDocks
               , layoutHook = myLayoutHook
               --, logHook = myLogHook xmproc
               --, handleEventHook = myHandleEventHook
               }  `additionalKeys` myKeys' -- `additionalMouseBindings` myMouseBindings


myManageHook = composeAll . concat $
    [ [ className =? "Transmission-gtk" --> doShift "9"]
    , [ className =? "Transmission-remote-gtk" --> doShift "9"]
    , [ (className =? "Firefox" <&&> resource =? "Dialog") --> doFloat]
    , [ className =? "Pidgin" --> doShift "9" ]
    , [ className =? "Xfce4-notifyd" --> doIgnore ]
    , [ (className =? "Thunderbird" <||> className =? "Icedove") --> doShift "8" ]
    , [ className =? "Anki" --> (doFloat <+> (doShift "7")) ]
    , [ className =? c --> doFloat | c <- myFloatsC ]
    ]
    where myFloatsC = ["org-spoutcraft-launcher-entrypoint-Start"
                      ,"Orage", "smPlayer"
                      ,"Nitrogen", "vlc", "mplayer", "Zenity"
                      ,"mplayer2", "GNOME MPlayer", "Linphone"
                      ]

-- -- myLogHook :: Handle -> X ()
-- -- myLogHook xmproc = dynamicLogWithPP xmobarPP
-- --     { ppOutput = hPutStrLn xmproc
-- --     -- , ppTitle = xmobarColor "grey" "" . shorten 50
-- --     ,ppTitle = shorten 80
-- --     }

myLayoutHook =
    maximize (tiled ||| Mirror tiled ||| Grid ||| spiral (6/7))
    ||| noBorders Full
    where tiled = Tall nmaster delta ratio
          nmaster = 1
          ratio = 1/2
          delta = 3/100

-- --myHandleEventHook = docksEventHook -- <+> fullscreenEventHook

-- -- use noModMask if you don't want a modmask. Yup
myKeys' :: [((ButtonMask, KeySym), X ())]
myKeys' = [ ((mod4Mask, xK_a), spawn "~/.xmonad/xmonad_menu")
          -- , ((mod4Mask .|. shiftMask, xK_h), sendMessage ToggleStruts)
          , ((mod4Mask, xK_f), withFocused (sendMessage . maximizeRestore))
          --, ((mod4Mask, xK_r), spawn "nowreading")
          , ((mod4Mask, xK_Print), scrot)
          , ((mod4Mask, xK_u), spawn "/usr/bin/slock")
          , ((noModMask, xK_Mute), mute)
          , ((noModMask, xK_VolUp), volumeUp 5)
          , ((noModMask, xK_VolDown), volumeDown 5)
          ]


xK_Mute :: KeySym
xK_Mute =  0x1008ff12

xK_VolDown :: KeySym
xK_VolDown = 0x1008ff11

xK_VolUp :: KeySym
xK_VolUp = 0x1008ff13

pulsectl :: [String] -> X ()
pulsectl xs = spawn $ intercalate " " (["/usr/bin/pulseaudio-ctl"] ++ xs)

mute :: X ()
mute = pulsectl ["mute"]

volumeUp :: Int -> X ()
volumeUp n = pulsectl ["up", (show n)]


volumeDown :: Int -> X ()
volumeDown n = pulsectl ["down", (show n)]

spawnXMonadScript :: String -> X ()
spawnXMonadScript s = spawn $ "~/.xmonad/xmonad_scripts/" ++ s

scrot :: X ()
scrot = spawn "scrot -e 'mv $f ~/pictures/scrots && viewnior ~/pictures/scrots/$f'"

--myMouseBindings :: [((ButtonMask, Button), Window -> X ())]
--myMouseBindings = [
--                   ((mod4Mask, 10), (\x -> spawn "notify-send \"heya\""))
--                  ]

--toggleMusic :: X ()
--toggleMusic = spawn "ncmpcpp toggle"
