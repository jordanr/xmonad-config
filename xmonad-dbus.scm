(define-module (gnu packages xmonad-dbus)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
  #:use-module (guix build-system haskell)
  #:use-module (gnu packages haskell-xyz))

(define-public xmonad-dbus
  (package
   (name "xmonad-dbus")
   (version "v0.1.0.2")
   (source (origin
            (method url-fetch)
            (uri (string-concatenate (list "https://github.com/troydm/xmonad-dbus/archive/refs/tags/" version ".tar.gz")))
            (sha256
             (base32 "0jq78b72jdq8qyr2am6mi2zih1iqb5drdy406w789x6galiklcc5"))))
   (build-system haskell-build-system)
   (arguments '(#:tests? #f))
   (inputs (list ghc-dbus))
   (home-page "https://hackage.haskell.org/package/xmonad-dbus")
   (synopsis "A library for connecting to DBus from XMonad")
   (description "xmonad-dbus is DBus monitoring solution inspired by xmonad-log completely written in Haskell. It allows you to easily send your status via DBus using XMonad's DynamicLog to any application that can execute custom scripts. It can be used to easily display XMonad status in polybar")
   (license license:bsd-3)))

xmonad-dbus
