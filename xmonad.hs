{-# LANGUAGE FlexibleContexts #-}
import XMonad
import XMonad.ManageHook
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageHelpers

import XMonad.Hooks.ManageDocks
-- import XMonad.Hooks.SetWMName
-- import XMonad.Util.Run (spawnPipe)
import XMonad.Util.EZConfig (additionalKeysP
                            ,additionalKeys
                            ,additionalMouseBindings
                            )
import XMonad.Layout.Grid
import XMonad.Layout.Spiral
import XMonad.Layout.Maximize
import XMonad.Layout.NoBorders (noBorders)
import XMonad.Layout.Spacing
import XMonad.Hooks.EwmhDesktops

import qualified Data.Map as M
import Data.List (intercalate)

import System.Posix.Env (getEnv)
import Data.Maybe (maybe)

import XMonad.Config.Desktop
import XMonad.Config.Gnome
import XMonad.Config.Kde
import XMonad.Config.Xfce

import qualified XMonad.DBus as D
import qualified DBus.Client as C
import qualified Codec.Binary.UTF8.String as UTF8
import System.IO
import Text.Printf

main = do
     session <- getEnv "DESKTOP_SESSION"
     let desktop_config = maybe desktopConfig desktop session
         sessionType = maybe "desktop" show session
       in startXmonad sessionType $ myConfig desktop_config

startXmonad :: (LayoutClass l Window, Read (l Window)) => String -> XConfig l -> IO ()
startXmonad "desktop" conf = startWithPolybar conf
startXmonad _ conf = xmonad conf
  
desktop "gnome" = gnomeConfig
desktop "kde" = kde4Config
desktop "xfce" = xfceConfig
desktop "xmonad-mate" = gnomeConfig
desktop _ = desktopConfig

startWithPolybar conf = do
  dbus <- D.connect
  D.requestAccess dbus
  xmonad $ conf {
    logHook = dynamicLogWithPP $ def { ppOutput = putStrLn }
    }
    

-- basicDesktop = desktopConfig {
--   startupHook = basicStartup >>= \_ -> startupHook desktopConfig
--   }
--   where basicStartup = do
--           -- spawn "xset rate r 240 30"
--           -- spawn "xrdb ~/.Xresources"
--           -- spawn "nitrogen --restore"
--           -- spawn "polybar"

myConfig desktop = desktop { borderWidth = 2
               ,modMask = mod4Mask
               ,terminal = "urxvt"
               ,normalBorderColor = "#cccccc"
               ,focusedBorderColor = "#336699" -- blue
               ,layoutHook = myLayoutHook
               }  `additionalKeys` myKeys'

myManageHook = composeAll . concat $
    [ [ className =? "deluge-gtk" --> doShift "9"]
    --, [ (className =? "Firefox" <&&> resource =? "Dialog") --> doFloat]
    , [ className =? "Pidgin" --> doShift "9" ]
    , [ className =? "Xfce4-notifyd" --> doIgnore ]
    , [ (className =? "Thunderbird" <||> className =? "Icedove") --> doShift "8" ]
    , [ className =? "Anki" --> (doFloat <+> (doShift "7")) ]
    , [ isDialog --> doCenterFloat ]
    , [ className =? c --> doFloat | c <- myFloatsC ]
    ]
    where myFloatsC = ["org-spoutcraft-launcher-entrypoint-Start"
                      ,"Orage", "smPlayer"
                      ,"nitrogen", "vlc", "mplayer", "Zenity"
                      ,"mplayer2", "GNOME MPlayer", "Linphone"
                      ,"SMplayer"
                      ]

-- -- myLogHook :: Handle -> X ()
-- -- myLogHook xmproc = dynamicLogWithPP xmobarPP
-- --     { ppOutput = hPutStrLn xmproc
-- --     -- , ppTitle = xmobarColor "grey" "" . shorten 50
-- --     ,ppTitle = shorten 80
-- --     }

myLayoutHook =
    avoidStruts $
    (spacingWithEdge 5 $ maximize (tiled ||| Mirror tiled ||| Grid ||| spiral (6/7)))
    ||| noBorders Full
    where tiled = Tall nmaster delta ratio
          nmaster = 1
          ratio = 1/2
          delta = 3/100

-- -- use noModMask if you don't want a modmask. Yup
myKeys' :: [((ButtonMask, KeySym), X ())]
myKeys' = [ 
            --((mod4Mask, xK_b), sendMessage ToggleStruts)
            ((mod4Mask, xK_b), toggle_bar)
          , ((mod4Mask, xK_f), withFocused
              (sendMessage . maximizeRestore))
          , ((mod4Mask, xK_m),
             withFocused (\_ -> sendMessage $ JumpToLayout "Full")
              >> hide_bar)
          , ((mod4Mask .|. controlMask, xK_t),
             toggleScreenSpacingEnabled >> toggleWindowSpacingEnabled)
          , ((mod4Mask .|. controlMask, xK_j), incWindowSpacing 5)
          , ((mod4Mask .|. controlMask, xK_k), decWindowSpacing 5)
          --, ((mod4Mask, xK_r), spawn "nowreading")
          , ((mod4Mask, xK_Print), scrot)
          , ((mod4Mask, xK_u), spawn "/usr/bin/slock")
          , ((mod4Mask, xK_p), spawn "rofi -show run")
          , ((mod4Mask, xK_w), spawn "rofi -show window")
          , ((noModMask, xK_VolUp), volumeUp 5)
          , ((noModMask, xK_VolDown), volumeDown 5)
          , ((noModMask, xK_Mute), volumeMute)
          , ((noModMask, xK_AudioMicMute), micMute)
          , ((noModMask, xK_BrightnessUp),
             spawn "brightnessctl set +10%")
          , ((noModMask, xK_BrightnessDown),
             spawn "brightnessctl --min-value=86 set 10%-")
          ]


xK_Mute :: KeySym
xK_Mute =  0x1008ff12

xK_VolDown :: KeySym
xK_VolDown = 0x1008ff11

xK_VolUp :: KeySym
xK_VolUp = 0x1008ff13

xK_BrightnessUp :: KeySym
xK_BrightnessUp = 0x1008ff02

xK_BrightnessDown :: KeySym
xK_BrightnessDown = 0x1008ff03

xK_AudioMicMute :: KeySym
xK_AudioMicMute = 0x1008ffb2

-- This is not working on fedora
pactl :: [String] -> X ()
pactl xs = spawn $ intercalate " " (["/usr/bin/pactl"] ++ xs)

volumeUp :: Int -> X ()
volumeUp n = pactl ["set-sink-volume", "@DEFAULT_SINK@",
                    (printf "+%d%%" n)]

volumeDown :: Int -> X ()
volumeDown n = pactl ["set-sink-volume", "@DEFAULT_SINK@",
                    (printf "-%d%%" n)]

volumeMute :: X ()
volumeMute = pactl ["set-sink-mute", "@DEFAULT_SINK@", "toggle"]

micMute :: X ()
micMute = pactl ["set-source-mute", "@DEFAULT_SOURCE@", "toggle"]

spawnXMonadScript :: String -> X ()
spawnXMonadScript s = spawn $ "~/.xmonad/xmonad_scripts/" ++ s

scrot :: X ()
scrot = spawn "scrot -e 'mv $f ~/pictures/scrots && nomacs ~/pictures/scrots/$f'"

toggle_bar :: X ()
toggle_bar = spawn "polybar-msg cmd toggle"

hide_bar :: X ()
hide_bar = spawn "polybar-msg cmd hide"

show_bar :: X ()
show_bar = spawn "polybar-msg cmd show"
-- setWallpaper :: String -> X ()
-- setWallpaper s = spawn $ "feh " ++ s ++ " --bg-scale"


