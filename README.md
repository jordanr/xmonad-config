# Xmonad Config
This is my xmonad config. I use it on my thinkpad running fedora 36, but it evolved from a config I've used since around 2011 on Debian. The bar I use with this config is currently [polybar](polybar.github.io)

I've now tried recreating this build on debian stable where it works pretty well. I've updated the steps following that experience.
## Getting started
If you'd like to mess around with my config, or xmonad in general, here are some tips.

### Using guix
I've been playing around with guix and I've added preliminary means to install my config using [guix](https://guix.gnu.org/).

1. Run `guix shell -f xmonad-dbus.scm -D xmonad-next ghc-xmonad-contrib-next`
   Alternatively run these by executing `./guix-activate.sh`
2. running `xmonad --recompile` should work now
3. You may not need these files once XMonad is compiled. You may choose to install all these into your guix profile, or you can set up your xmonad `build` script to compile it using the guix shell

I'm still learning how to use guix so these steps may or may not work in your case. Take it slow, computers can be hard.
### Using ghcup and stack
1. Get ghcup [ghcup](https://www.haskell.org/ghcup/ "the easiest tool")
   It's probably easier and less likely to break than your distro's ghc
3. run `git clone https://gitlab.com/jordanr/xmonad-config ~/.xmonad`
3. This project contains several submodules; initialize the submodules with `git submodule update --init` from the top level of the directory.
4. You'll need to install [polybar](polybar.github.io) for your distro.
5. Additionally you need to install the xmonad dependencies for your distro [see what they are in xmonad's documentation](https://xmonad.org/INSTALL.html#dependencies)
6. You're now ready to install. Run `stack install` (say a little prayer)
7. You'll need to use `stack ghc` and `stack repl` to mess around with things. `xmonad --recompile` will automatically detec stack and act appropriately.
8. Happy hacking!
